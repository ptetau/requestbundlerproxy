using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Tests
{
    public class TestApiEndpointTests : BaseTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task MultEndPoint()
        {
            var client = GetTestApiHttpClient();


            var formData = new Dictionary<string, string>
            {
                {"A", "2"},
                {"B", "5"}
            };

            var postRequest = ToJsonHttpRequestMessage(formData, "/api/mult/");

            var response = await client.SendAsync(postRequest);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        [Test]
        public async Task MultGetEndPoint()
        {
            var client = GetTestApiHttpClient();

            var postRequest = new HttpRequestMessage(HttpMethod.Get, "/api/mult/2/3");

            var response = await client.SendAsync(postRequest);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        [Test]
        public async Task SquareRootEndPoint()
        {
            var client = GetTestApiHttpClient();

            var postRequest = ToJsonHttpRequestMessage(JsonConvert.SerializeObject(9), "/api/squareroot/");

            var response = await client.SendAsync(postRequest);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        [Test]
        public async Task SquareRootGetEndPoint()
        {
            var client = GetTestApiHttpClient();

            var postRequest = new HttpRequestMessage(HttpMethod.Get, "/api/squareroot/9");

            var response = await client.SendAsync(postRequest);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }
        
        [Test]
        public async Task SquareEndPoint()
        {
            var client = GetTestApiHttpClient();

            var postRequest = ToJsonHttpRequestMessage(JsonConvert.SerializeObject(9), "/api/square/");

            var response = await client.SendAsync(postRequest);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        [Test]
        public async Task SquareGetEndPoint()
        {
            var client = GetTestApiHttpClient();

            var postRequest = new HttpRequestMessage(HttpMethod.Get, "/api/square/9");

            var response = await client.SendAsync(postRequest);

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }
    }
}