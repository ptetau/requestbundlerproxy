using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using RequestBundlerProxy;

namespace Tests
{
    public class RequestQueryBundlerTests : BaseTest
    {
        [Test]
        public async Task OkayRequest()
        {
            string requestBundle = @"{
              ""Requests"": [
                {
                  ""Name"": ""test_request"",
                  ""Url"": ""http://localhost/api/squareroot/"",
                  ""Body"": ""4"",
                  ""Action"": ""POST"",
                  ""Outputs"": [
                    {
                      ""Name"": ""id"",
                      ""JsonPath"": ""$.squareRoot""
                    }
                  ]
                }
              ]
            }";

            var httpClient = GetRqbHttpClient();
            var jsonHttpRequestMessage = ToJsonHttpRequestMessage(requestBundle, "http://localhost/api/requestbundle");
            var res = await httpClient.SendAsync(jsonHttpRequestMessage);
            res.EnsureSuccessStatusCode();

            var output = await res.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<dynamic>(output);

            var ss = json.ToString();
            Assert.That(json.successfulResponses.Count == 1);
            Assert.That(json.successfulResponses[0].outputs.id == 2);
        }

        [Test]
        public async Task BadRequest()
        {
            string requestBundle = @"{
              ""Requests"": [
                {
                  ""Name"": ""test_request"",
                  ""Url"": ""http://localhost/api/does_not_exist/"",
                  ""Body"": ""4"",
                  ""Action"": ""POST"",
                  ""Outputs"": [
                    {
                      ""Name"": ""id"",
                      ""JsonPath"": ""$.squareRoot""
                    }
                  ]
                }
              ]
            }";

            var httpClient = GetRqbHttpClient();
            var jsonHttpRequestMessage = ToJsonHttpRequestMessage(requestBundle, "http://localhost/api/requestbundle");
            var res = await httpClient.SendAsync(jsonHttpRequestMessage);
            res.EnsureSuccessStatusCode();

            var output = await res.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<dynamic>(output);

            var ss = json.ToString();
            Assert.AreEqual(0, json.successfulResponses.Count, "Expected 0 successes");
            Assert.AreEqual(1, json.failedResponses.Count, "Expected 1 failure");
            Assert.AreEqual("{'HttpStatus':'NotFound','error':''}", json.failedResponses[0].content.ToString());
        }

        [Test]
        public async Task ServiceUnavailable()
        {
            var builder = new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<RequestBundlerProxy.Startup>()
                .ConfigureTestServices(services =>
                    {
                        services.AddTransient<IHttpClient, UnavailableServiceHttpClient>();
                    }
                );

            TestServer testServer = new TestServer(builder);

            HttpClient httpClient = testServer.CreateClient();


            string requestBundle = @"{
              ""Requests"": [
                {
                  ""Name"": ""test_request"",
                  ""Url"": ""http://localhost/api/does_not_exist/"",
                  ""Body"": ""4"",
                  ""Action"": ""POST"",
                  ""Outputs"": [
                    {
                      ""Name"": ""id"",
                      ""JsonPath"": ""$.squareRoot""
                    }
                  ]
                }
              ]
            }";


            var jsonHttpRequestMessage = ToJsonHttpRequestMessage(requestBundle, "http://localhost/api/requestbundle");
            var res = await httpClient.SendAsync(jsonHttpRequestMessage);
            res.EnsureSuccessStatusCode();

            var output = await res.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<dynamic>(output);

            var ss = json.ToString();
            Assert.AreEqual(0, json.successfulResponses.Count, "Expected 0 successes");
            Assert.AreEqual(1, json.failedResponses.Count, "Expected 1 failure");
            Assert.True(json.failedResponses[0].content.ToString().StartsWith("{'HttpStatus':'NotFound','error"));
        }

        [Test]
        public async Task TwoRequests()
        {
            string requestBundle = @"{
              ""Requests"": [
                {
                  ""Name"": ""test_request"",
                  ""Url"": ""http://localhost/api/squareroot/"",
                  ""Body"": ""4"",
                  ""Action"": ""POST"",
                  ""Outputs"": [
                    {
                      ""Name"": ""sqr_4"",
                      ""JsonPath"": ""$.squareRoot""
                    }
                  ]
                },
                {
                  ""Name"": ""test_request1"",
                  ""Url"": ""http://localhost/api/mult/3/7"",                 
                  ""Action"": ""GET"",
                  ""Outputs"": [
                    {
                      ""Name"": ""id2"",
                      ""RegExp"": "".*""
                    }
                  ]
                }
              ]
            }";

            var httpClient = GetRqbHttpClient();
            var jsonHttpRequestMessage = ToJsonHttpRequestMessage(requestBundle, "http://localhost/api/requestbundle");
            var res = await httpClient.SendAsync(jsonHttpRequestMessage);
            res.EnsureSuccessStatusCode();

            var output = await res.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<dynamic>(output);

            var ss = json.ToString();
            Assert.AreEqual(2, json.successfulResponses.Count, "Expect 2 successes");
            Assert.AreEqual("2", json.successfulResponses[0].outputs.sqr_4.ToString(), "Square root of 4 equals 2");
        }
    }
}