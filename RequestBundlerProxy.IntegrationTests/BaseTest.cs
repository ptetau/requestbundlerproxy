using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RequestBundlerProxy;

namespace Tests
{
    public class BaseTest
    {
        private static HttpClient RqbHttpClient;
        private static HttpClient TestApiHttpClient;

        protected static HttpClient GetTestApiHttpClient()
        {
            if (TestApiHttpClient != null)
            {
                return TestApiHttpClient;
            }

            var builder = new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<TestApi.Startup>()
                .ConfigureTestServices(services => { services.AddTransient<HttpClient, HttpClient>(); }
                );

            TestServer testServer = new TestServer(builder);

            TestApiHttpClient = testServer.CreateClient();
            return TestApiHttpClient;
        }

        protected static HttpClient GetRqbHttpClient()
        {
            if (RqbHttpClient != null)
            {
                return RqbHttpClient;
            }

            var builder = new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<RequestBundlerProxy.Startup>()
                .ConfigureTestServices(services => { services.AddTransient<IHttpClient, BogusHttpClient>(); }
                );

            TestServer testServer = new TestServer(builder);

            RqbHttpClient = testServer.CreateClient();
            return RqbHttpClient;
        }

        protected HttpRequestMessage ToJsonHttpRequestMessage(string s, string requestUri)
        {
            HttpRequestMessage postRequest = new HttpRequestMessage(HttpMethod.Post, requestUri)
            {
                Content = new StringContent(s, Encoding.UTF8, "application/json")
            };
            return postRequest;
        }

        protected static HttpRequestMessage ToJsonHttpRequestMessage(Dictionary<string, string> formData,
            string requestUri)
        {
            HttpRequestMessage postRequest = new HttpRequestMessage(HttpMethod.Post, requestUri)
            {
                Content = new StringContent(JsonConvert.SerializeObject(formData), Encoding.UTF8, "application/json")
            };
            return postRequest;
        }

        protected static HttpRequestMessage ToHttpRequestMessage(object formData, string requestUri)
        {
            HttpRequestMessage postRequest = new HttpRequestMessage(HttpMethod.Post, requestUri)
            {
                Content = new StringContent(formData.ToString(), Encoding.UTF8, "application/x-www-form-urlencoded")
            };
            return postRequest;
        }
    }
}