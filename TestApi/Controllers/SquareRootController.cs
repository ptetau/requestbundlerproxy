﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace TestApi.Controllers
{
    [Route("api/[controller]")]
    public class SquareRootController : Controller
    {
        // GET api/squareroot/5
        [HttpGet("{value}")]
        public double Get(double value)
        {
            return Math.Sqrt(value);
        }

        // POST api/squareroot
        [HttpPost]
        public OkObjectResult Post([FromBody] double value)
        {
            return Ok(new {SquareRoot = Math.Sqrt(value)});
        }
    }
}