﻿using Microsoft.AspNetCore.Mvc;

namespace TestApi.Controllers
{
    [Route("api/[controller]")]
    public class MultController : Controller
    {
        [HttpGet("{valueA}/{valueB}")]
        public OkObjectResult Get(double valueA, double valueB)
        {
            return Ok(new {A = valueA, B = valueB, product = valueA * valueB});
        }

        [HttpPost]
        public OkObjectResult Post([FromBody] Ab value)
        {
            return Ok(new {A = value.A, B = value.B, product = ((value.A)) * ((value.B))});
        }
    }
}