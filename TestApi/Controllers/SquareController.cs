﻿using Microsoft.AspNetCore.Mvc;

namespace TestApi.Controllers
{
    [Route("api/[controller]")]
    public class SquareController : Controller
    {
        [HttpGet("{value}")]
        public double Get(double value)
        {
            return value * value;
        }

        [HttpPost]
        public OkObjectResult Post([FromBody] double value)
        {
            return Ok(new {square = value * value});
        }
    }

    public class Ab
    {        
        public double A { get;  set; }
        public double B { get;  set; }
    }
}