﻿using System.Net.Http;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.CodeAnalysis;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RequestBundlerProxy.Schemas.Request;
using RequestBundlerProxy.Services;
using RequestBundlerProxy.ViewModels;

namespace RequestBundlerProxy
{
    public class Startup
    {
        private static readonly object MapLock = new object();

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
//              .AddJsonFile("appsettings.json", false, true)
//              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        private IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddTransient<HttpClient, HttpClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            LoadAutomaticMappings();

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }

        private static void LoadAutomaticMappings()
        {
            if (HasMappings) return;
            lock (MapLock)
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<RequestModel, Request>();
                    cfg.CreateMap<RequestBundleModel, RequestBundle>();
                });
                HasMappings = true;
            }
        }

        private static bool HasMappings;
    }
}