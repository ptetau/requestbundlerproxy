﻿using System.Collections.Generic;

namespace RequestBundlerProxy.Schemas.Responses
{
    public sealed class Response
    {
        public Response(Request.Request request, string name, IReadOnlyDictionary<string, string> outputs, string content)
        {
            Request = request;
            Content = content;
            Name = name;
            Outputs = outputs;
        }

        public Response(string name, Request.Request request, string content)
        {
            Name = name;
            Request = request;
            Content = content;
        }

        public Request.Request Request { get; }
        public string Content { get; }
        public string Name { get; }
        public IReadOnlyDictionary<string, string> Outputs { get; }
    }
}