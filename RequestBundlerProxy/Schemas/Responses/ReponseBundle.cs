﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace RequestBundlerProxy.Schemas.Responses
{
    public sealed class ReponseBundle
    {
        private readonly List<(bool success, Response response)> responses;

        public ReponseBundle(List<(bool success, Response response)> responses)
        {
            this.responses = responses;
        }

        public IReadOnlyList<(bool success, Response response)> Responses => responses.ToImmutableList();
    }
}