﻿using RequestBundlerProxy.Parsers;

namespace RequestBundlerProxy.Schemas.Request
{
    // ReSharper disable once ClassNeverInstantiated.Global

    public sealed class OutputExtractor
    {
        public string Name { get; set; }
        public string JsonPath { get; set; }
        public string RegExp { get; set; }

        public string Output(string responseBody)
        {
            string output = null;
            if (RegExp != null)
                output = RegExParser.Parse(RegExp, responseBody);
            else if (JsonPath != null) output = JsonParser.Parse(JsonPath, responseBody);

            return output;
        }
    }
}