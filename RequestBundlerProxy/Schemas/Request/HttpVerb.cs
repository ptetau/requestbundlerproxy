﻿namespace RequestBundlerProxy.Schemas.Request
{
    public enum HttpVerb
    {
        Post,
        Get
    }
}