﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using RequestBundlerProxy.Extensions;

namespace RequestBundlerProxy.Schemas.Request
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class Request
    {
        private static readonly Regex ParameterPattern = new Regex(@"\{(?<param>\w+)\}", RegexOptions.Compiled);

        public string Name { get; set; }
        public string Url { get; set; }

        public HttpVerb Action { get; set; }

        public Dictionary<string, string> FormBody { get; set; } = new Dictionary<string, string>();
        public string Body { get; set; } = string.Empty;

        public List<OutputExtractor> Outputs { get; set; } = new List<OutputExtractor>();

        private List<string> BodyWildCards => ExtractWildCards(Body);
        private List<string> FormWildCards => GetWildCards(FormBody);
        private List<string> UrlWildCards => ExtractWildCards(Url);

        public bool HasInputs(IReadOnlyDictionary<string, string> inputs)
        {
            switch (Action)
            {
                case HttpVerb.Post:
                    List<string> wildcards = FormWildCards.Concat(BodyWildCards).ToHashSet().ToList();
                    return !wildcards.Any() || inputs.Keys.ContainsAll(wildcards);
                case HttpVerb.Get:
                    return inputs.Keys.ContainsAll(UrlWildCards);
            }

            return false;
        }

        private static List<string> GetWildCards(IDictionary<string, string> form)
        {
            var wildCards = new List<string>();
            if (form == null) return wildCards;

            //De-dup
            ImmutableHashSet<string> immutableFormValues = form.Values.ToImmutableHashSet();
            foreach (var value in immutableFormValues)
                //only concerned about the first match
                wildCards.AddRange(ExtractWildCards(value));

            return wildCards;
        }

        private static List<string> ExtractWildCards(string str)
        {
            var wildCards = new List<string>();
            if (str == null) return wildCards;

            foreach (Match match in ParameterPattern.Matches(str)) wildCards.Add(match.Groups["param"].Value);

            return wildCards.ToImmutableHashSet().ToList();
        }
    }
}