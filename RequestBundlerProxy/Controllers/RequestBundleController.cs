﻿using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RequestBundlerProxy.Services;
using RequestBundlerProxy.ViewModels;

namespace RequestBundlerProxy.Controllers
{
    [Route("api/[controller]")]
    public class RequestBundleController : Controller
    {
        private readonly IHttpClient httpClient;

        public RequestBundleController(IHttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        [HttpPost]
        public async Task<ReponseBundleModel> MultipleRequests([FromBody] RequestBundleModel requestBundleModel)
        {
            var requestBundle = Mapper.Map<RequestBundle>(requestBundleModel);
            var responseBundle = await new RequestBundleProcessor().Process(requestBundle, httpClient);
            var responseBundleModel = new ResponseBundleModelMapper().Map(responseBundle);
            return responseBundleModel;
        }
    }
}