﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using RequestBundlerProxy.Extensions;
using RequestBundlerProxy.Schemas.Request;
using RequestBundlerProxy.Schemas.Responses;

namespace RequestBundlerProxy.Services
{
    internal class RequestBundleProcess
    {
        private readonly HttpRequestDispatcher requestDispatcher;
        private readonly ResponseOutputExtractor responseOutputExtractor;

        public RequestBundleProcess(HttpRequestDispatcher requestDispatcher,
            ResponseOutputExtractor responseOutputExtractor)
        {
            this.requestDispatcher = requestDispatcher;
            this.responseOutputExtractor = responseOutputExtractor;
        }

        private BundleState State { get; set; } = BundleState.Default;

        public async Task<ReponseBundle> Process(ImmutableList<Request> requests)
        {
            if (State == BundleState.Completed)
            {
                return new ReponseBundle(new List<(bool success, Response response)>());
            }

            var responses = await Run(requests);
            return new ReponseBundle(responses);
        }

        /// <summary>
        /// This is a naive dependent request runner.
        /// 1) We start by creating a list of requests that are runnable, i.e. have and dependencies ready.
        /// 2) Wait for those requests to return.
        /// 3) Extract and outputs to the collection of dependencies
        /// 4) Check to see if we have stalled e.g. requests are not resolving
        /// 5) loop back to step 1)
        /// </summary>
        /// <param name="requests"></param>
        private async Task<List<(bool success, Response response)>> Run(ImmutableList<Request> requests)
        {
            IReadOnlyDictionary<string, string> dependencies = new Dictionary<string, string>();
            var previousRequestCount = 0;
            var remainingRequests = new List<Request>(requests);
            var allResponses = new List<(bool success, Response response)>();

            while (remainingRequests.Any())
            {
                /*Ready requests*/
                var runnableRequests = remainingRequests
                    .Where(request => request.HasInputs(dependencies))
                    .ToList();

                var requestTasks =
                    CreateRequestDispatchTasks(runnableRequests, dependencies);

                /*Dispatch request*/
                var responses = await ExecuteInParallel(requestTasks);

                /*Update responses*/
                allResponses.AddRange(responses);

                /*Update dependencies*/
                dependencies = AddOutputToDependencies(responses, dependencies);

                var completedRequests = responses
                    .Select(tuple => tuple.response.Request);


                /*Update requests*/
                remainingRequests = remainingRequests.Except(completedRequests).ToList();


                /*Exit condition*/
                if (remainingRequests.Count == previousRequestCount)
                {
                    /*Requests are not completing*/
                    break;
                }

                previousRequestCount = remainingRequests.Count;
            }

            State = BundleState.Completed;
            return allResponses;
        }

        private static async Task<IReadOnlyList<(bool success, Response response)>> ExecuteInParallel(
            IReadOnlyList<Task<(bool success, Response)>> requestTasks)
        {
            await Task.WhenAll(requestTasks);

            IReadOnlyList<(bool success, Response response)> responses = requestTasks
                .Where(task => task.IsCompleted)
                .Select(task => task.Result)
                .ToImmutableList();
            return responses;
        }

        private static IReadOnlyDictionary<string, string> AddOutputToDependencies(
            IReadOnlyList<(bool success, Response response)> responses,
            IReadOnlyDictionary<string, string> dependencies)
        {
            var dependenciesOut = new Dictionary<string, string>(dependencies);

            IEnumerable<IReadOnlyDictionary<string, string>> outputs =
                from res in responses
                where res.success
                select res.response.Outputs;

            foreach (IReadOnlyDictionary<string, string> output in outputs)
            {
                dependenciesOut.AddAllKvPairs(output);
            }

            return dependenciesOut;
        }

        private IReadOnlyList<Task<(bool success, Response)>> CreateRequestDispatchTasks(List<Request> runnableRequests,
            IReadOnlyDictionary<string, string> dependencies)
        {
            return runnableRequests
                .Select(async request => await Dispatch(request, dependencies))
                .ToImmutableList();
        }

        private async Task<(bool, Response)> Dispatch(Request request, IReadOnlyDictionary<string, string> dependencies)
        {
            var (success, content) = await requestDispatcher.Dispatch(request, dependencies);

            return success
                ? (true, responseOutputExtractor.ProcessRequest(request, content))
                : (false, new Response(request.Name, request, content));
        }

        private enum BundleState
        {
            Default,
            Completed
        }
    }
}