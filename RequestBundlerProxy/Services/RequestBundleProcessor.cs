﻿using System.Net.Http;
using System.Threading.Tasks;
using RequestBundlerProxy.Schemas.Responses;

namespace RequestBundlerProxy.Services
{
    internal class RequestBundleProcessor
    {
        public async Task<ReponseBundle> Process(RequestBundle requestBundle, IHttpClient httpClient)
        {           
            var requestDispatcher = new HttpRequestDispatcher(httpClient);
            requestDispatcher.Configure(requestBundle);

            var responseOutputExtractor = new ResponseOutputExtractor();
            var responses = await requestBundle.Process(requestDispatcher, responseOutputExtractor);

            return responses;
        }
    }
}