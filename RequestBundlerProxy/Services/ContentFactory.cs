﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using RequestBundlerProxy.Extensions;

namespace RequestBundlerProxy.Services
{
    public static class ContentFactory
    {
        public static (bool success, StringContent content) Create(Dictionary<string, string> formBody,
            IReadOnlyDictionary<string, string> parameterBag)
        {
            if (formBody.Keys.Count == 0) return (false, null);

            IEnumerable<KeyValuePair<string, string>> templatedParameters =
                formBody.Select(pair => KeyValuePair.Create(pair.Key, pair.Value.FormatWith(parameterBag)))
                    .ToDictionary();

            var serializeObject = JsonConvert.SerializeObject(templatedParameters);
            return (true, new StringContent(
                serializeObject,
                Encoding.UTF8,
                "application/json"
            ));
        }

        public static (bool success, StringContent content) Create(string body,
            IReadOnlyDictionary<string, string> parameterBag)
        {
            if (string.IsNullOrEmpty(body)) return (false, null);

            var templatedBody = body.FormatWith(parameterBag);

            return (true, new StringContent(
                templatedBody,
                Encoding.UTF8,
                "application/json"
            ));
        }
    }
}