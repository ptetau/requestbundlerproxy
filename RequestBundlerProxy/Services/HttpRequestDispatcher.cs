﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using RequestBundlerProxy.Extensions;
using RequestBundlerProxy.Schemas.Request;

namespace RequestBundlerProxy.Services
{
    public class HttpRequestDispatcher
    {
        private readonly IHttpClient httpClient;

        public HttpRequestDispatcher(IHttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public void Configure(RequestBundle requestBundle)
        {
            if (requestBundle.Headers == null) return;

            foreach (var key in requestBundle.Headers.Keys)
                httpClient.DefaultRequestHeaders.Add(key, requestBundle.Headers[key]);
        }

        public async Task<(bool success, string body)> Dispatch(Request request,
            IReadOnlyDictionary<string, string> parameterBag)
        {
            try
            {
                HttpResponseMessage httpResponseMessage;

                try
                {
                    switch (request.Action)
                    {
                        case HttpVerb.Get:
                            httpResponseMessage = await AsyncGet(request, parameterBag);
                            break;
                        case HttpVerb.Post:
                            httpResponseMessage = await AsyncPost(request, parameterBag);
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                }
                catch (HttpRequestException e)
                {
                    return (false, $"{{'error':'{e.Message}'}}");
                }

                if (!httpResponseMessage.IsSuccessStatusCode)
                    return (false,
                        $"{{'HttpStatus':'{httpResponseMessage.StatusCode}','error':'{await httpResponseMessage?.Content.ReadAsStringAsync() ?? httpResponseMessage.ReasonPhrase}'}}");

                var stringResult = await httpResponseMessage.Content.ReadAsStringAsync();

                return (true, stringResult);
            }
            catch (HttpRequestException httpRequestException)
            {
                return (false, $"{{'error':'{httpRequestException.Message}'}}");
            }
        }

        private async Task<HttpResponseMessage> AsyncGet(Request request, IReadOnlyDictionary<string, string> parameterBag)
        {
            var parameters = new Dictionary<string, string>(request.FormBody.ToDictionary());
            parameters.AddAllKvPairs(parameterBag);

            var requestUrl = request.Url != null
                ? request.Url.FormatWith(parameters)
                : QueryHelpers.AddQueryString(request.Url, parameters);


            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            return await httpClient.SendAsync(httpRequestMessage);
        }

        private async Task<HttpResponseMessage> AsyncPost(Request request, IReadOnlyDictionary<string, string> parameterBag)
        {
            var requestUrl = request.Url;

            var (success, content) =
                ContentFactory.Create(request.FormBody, parameterBag)
                    .Or(ContentFactory.Create(request.Body, parameterBag));

            HttpRequestMessage httpRequestMessage;
            if (success)
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, requestUrl)
                {
                    Content = content
                };
            else
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, requestUrl);

            return await httpClient.SendAsync(httpRequestMessage);
        }
    }
}