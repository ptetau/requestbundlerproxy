﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using RequestBundlerProxy.Schemas.Request;
using RequestBundlerProxy.Schemas.Responses;

namespace RequestBundlerProxy.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class RequestBundle
    {
        public Request[] Requests { get; set; }
        public IDictionary<string, string> Headers { get; set; }

        public Task<ReponseBundle> Process(HttpRequestDispatcher requestDispatcher,
            ResponseOutputExtractor responseOutputExtractor)
        {
            var bundleProgress = new RequestBundleProcess(requestDispatcher,responseOutputExtractor);
            return bundleProgress.Process(Requests.ToImmutableList());
        }
    }
}