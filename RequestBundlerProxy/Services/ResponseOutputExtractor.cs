﻿using System.Collections.Generic;
using System.Collections.Immutable;
using RequestBundlerProxy.Schemas.Request;
using RequestBundlerProxy.Schemas.Responses;

namespace RequestBundlerProxy.Services
{
    public sealed class ResponseOutputExtractor
    {
        public Response ProcessRequest(Request request, string content)
        {
            var name = request.Name;
            ImmutableDictionary<string, string> outputs = new Dictionary<string, string>().ToImmutableDictionary();

            if (request.Outputs != null) outputs = ExtractOutputs(request.Outputs, content);

            return new Response(request, name, outputs, content);
        }

        private ImmutableDictionary<string, string> ExtractOutputs(IList<OutputExtractor> outputExtractors,
            string responseBody)
        {
            ImmutableDictionary<string, string> outputs = outputExtractors.ToImmutableDictionary(
                extractor => extractor.Name,
                extractor => extractor.Output(responseBody));

            return outputs;
        }
    }
}