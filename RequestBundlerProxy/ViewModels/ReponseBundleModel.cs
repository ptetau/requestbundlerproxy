﻿using Newtonsoft.Json;

namespace RequestBundlerProxy.ViewModels
{
    [JsonObject(MemberSerialization.OptOut)]
    public sealed class ReponseBundleModel
    {
        public ReponseBundleModel(ResponseModel[] successfulResponses, ResponseModel[] failedResponses)
        {
            SuccessfulResponses = successfulResponses;
            FailedResponses = failedResponses;
        }

        public ResponseModel[] SuccessfulResponses { get; }
        public ResponseModel[] FailedResponses { get; }
    }
}