﻿using System.ComponentModel.DataAnnotations;

namespace RequestBundlerProxy.ViewModels
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class OutputExtractorModel
    {
        [Required] public string Name { get; set; }

        public string JsonPath { get; set; }
        public string RegExp { get; set; }
    }
}