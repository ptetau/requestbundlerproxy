﻿using System.Collections.Generic;

namespace RequestBundlerProxy.ViewModels
{
    public sealed class RequestBundleModel
    {
        public RequestModel[] Requests { get; set; }
        public Dictionary<string, string> Headers { get; set; }
    }
}