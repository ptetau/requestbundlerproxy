﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RequestBundlerProxy.Schemas.Request;

namespace RequestBundlerProxy.ViewModels
{
    [JsonObject(MemberSerialization.OptOut)]
    public sealed class RequestModel
    {
        [Required] public string Name { get; set; }
        [Required] public string Url { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public HttpVerb Action { get; set; }

        public Dictionary<string, string> FormBody { get; set; } = new Dictionary<string, string>();
        public string Body { get; set; } = string.Empty;

        public List<OutputExtractorModel> Outputs { get; set; } = new List<OutputExtractorModel>();
    }
}