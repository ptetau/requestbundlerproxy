﻿using System.Linq;
using AutoMapper;
using RequestBundlerProxy.Schemas.Responses;

namespace RequestBundlerProxy.ViewModels
{
    public sealed class ResponseBundleModelMapper
    {
        public ReponseBundleModel Map(ReponseBundle responseBundle)
        {
            ResponseModel[] successfulResponses = responseBundle
                    .Responses
                    .Where(tuple => tuple.success)
                    .Select(tuple => Mapper.Map<ResponseModel>(tuple.response))
                    .ToArray();

            ResponseModel[] failedResponses = responseBundle
                    .Responses
                    .Where(tuple => !tuple.success)
                    .Select(tuple => Mapper.Map<ResponseModel>(tuple.response))
                    .ToArray();

            return new ReponseBundleModel(successfulResponses, failedResponses);
        }
    }
}