﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RequestBundlerProxy.ViewModels
{
    [JsonObject(MemberSerialization.OptOut)]
    public sealed class ResponseModel
    {
        [JsonIgnore] 
        public RequestModel Request { get; set; }

        public string Name { get; set; }
        public string Content { get; set; }
        public IDictionary<string, string> Outputs { get; set; }
    }
}