﻿using System.Collections.Concurrent;
using Newtonsoft.Json.Linq;

namespace RequestBundlerProxy.Parsers
{
    public static class JsonParser
    {
        private static readonly ConcurrentDictionary<string, JObject> JsonCache =
            new ConcurrentDictionary<string, JObject>();

        public static string Parse(string derivativeExpression, string responseBody)
        {
            var cachedBody = JsonCache.GetOrAdd(responseBody, s => JObject.Parse(responseBody));

            return cachedBody.SelectToken(derivativeExpression).ToString();
        }
    }
}