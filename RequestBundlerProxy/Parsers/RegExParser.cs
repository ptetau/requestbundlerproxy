﻿using System.Collections.Concurrent;
using System.Text.RegularExpressions;

namespace RequestBundlerProxy.Parsers
{
    public static class RegExParser
    {
        private static readonly ConcurrentDictionary<string, Regex> RegExCache =
            new ConcurrentDictionary<string, Regex>();

        public static string Parse(string expression, string body)
        {
            if (RegExCache.Keys.Contains(expression)) return RegExCache[expression].Match(body).Value;

            var regex = new Regex(expression, RegexOptions.Compiled);
            RegExCache.TryAdd(expression, regex);

            return regex.Match(body).Value;
        }
    }
}