﻿using System.Collections.Generic;
using System.Linq;

namespace RequestBundlerProxy.Extensions
{
    public static class EnumerableExtentions
    {
        public static bool ContainsAllKeys<TK, TV>(this IReadOnlyDictionary<TK, TV> items,
            IReadOnlyDictionary<TK, TV> others)
        {
            List<TK> itemz = items.Keys.ToList();
            return others.Keys.All(o => itemz.Contains(o));
        }

        public static bool ContainsAll<TV>(this IEnumerable<TV> items, IEnumerable<TV> others)
        {
            List<TV> itemz = items.ToList();
            return others.All(o => itemz.Contains(o));
        }
    }
}