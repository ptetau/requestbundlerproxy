﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RequestBundlerProxy.Extensions
{
    public static class KeyValuePairCollectionExtensions
    {
        public static Dictionary<string, string> ToDictionary(this IEnumerable<KeyValuePair<string, string>> keyValues)
        {
            if (keyValues == null) throw new ArgumentNullException(nameof(keyValues));
            return keyValues.ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }
}