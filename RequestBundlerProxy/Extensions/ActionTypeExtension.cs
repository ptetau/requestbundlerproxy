﻿using System;
using System.Net.Http;

namespace RequestBundlerProxy.Extensions
{
    public static class ActionTypeExtension
    {
        public static HttpMethod ToHttpMethod(this string actionType)
        {
            switch (actionType.ToLower())
            {
                case "get":
                    return HttpMethod.Get;
                case "post":
                    return HttpMethod.Post;
                default:
                    throw new ArgumentOutOfRangeException(nameof(actionType), actionType, null);
            }
        }
    }
}