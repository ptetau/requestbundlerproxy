﻿namespace RequestBundlerProxy.Extensions
{
    public static class TupleExtension
    {
        public static (bool success, T1 v1) Or<T1>(this (bool success, T1 v1) a, (bool success, T1 v1) b)
        {
            return a.success ? a : b;
        }

        public static (bool success, T1, T2 v2) Or<T1, T2>(this (bool success, T1, T2 v2) a,
            (bool success, T1, T2 v2) b)
        {
            return a.success ? a : b;
        }

        public static (bool success, T1 v1, T2 v2, T3 v3) Or<T1, T2, T3>(this (bool success, T1 v1, T2 v2, T3 v3) a,
            (bool success, T1 v1, T2 v2, T3 v3) b)
        {
            return a.success ? a : b;
        }

        public static (bool success, T1 v1, T2 v2, T3 v3, T4 t4) Or<T1, T2, T3, T4>(
            this (bool success, T1 v1, T2 v2, T3 v3, T4 t4) a, (bool success, T1 v1, T2 v2, T3 v3, T4 t4) b)
        {
            return a.success ? a : b;
        }
    }
}