﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RequestBundlerProxy.Extensions
{
    public static class StringExtension
    {
        public static string FormatWith(this string format, IReadOnlyDictionary<string, string> source)
        {
            return FormatWith(format, null, source);
        }

        private static string FormatWith(this string format, IFormatProvider provider,
            IReadOnlyDictionary<string, string> source)
        {
            if (format == null)
                throw new ArgumentNullException(nameof(format));

            var r = new Regex(@"(?<start>\{)+(?<property>[\w\.\[\]]+)(?<format>:[^}]+)?(?<end>\})+",
                RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

            var values = new List<object>();
            var rewrittenFormat = r.Replace(format, delegate(Match m)
            {
                var startGroup = m.Groups["start"];
                var propertyGroup = m.Groups["property"];
                var formatGroup = m.Groups["format"];
                var endGroup = m.Groups["end"];

                values.Add(propertyGroup.Value == "0"
                    ? source
                    : Eval(source, propertyGroup.Value));

                return new string('{', startGroup.Captures.Count) + (values.Count - 1) + formatGroup.Value
                       + new string('}', endGroup.Captures.Count);
            });

            return string.Format(provider, rewrittenFormat, values.ToArray());
        }


        private static object Eval(IReadOnlyDictionary<string, string> source, string propertyGroupValue)
        {
            return source.ContainsKey(propertyGroupValue) ? source[propertyGroupValue] : string.Empty;
        }
    }
}