﻿using System.Collections.Generic;

namespace RequestBundlerProxy.Extensions
{
    public static class DictionaryExtensions
    {
        public static void AddAllKvPairs<TK, TV>(this IDictionary<TK, TV> dict, IReadOnlyDictionary<TK, TV> add)
        {
            foreach (var kv in add)
            {                
                if (dict.ContainsKey(kv.Key))
                {
                    //Update 
                    dict.Remove(kv);
                    dict.Add(kv.Key, kv.Value);
                }
                else
                {
                    dict.Add(kv.Key, kv.Value);
                }

                
            }
        }
    }
}